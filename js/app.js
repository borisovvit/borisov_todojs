/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
$(() => {
  const ENTER_KEY = 'Enter';
  let todos = [];
  let error = false;
  let tabMode = 'all';
  let currentPage = 1;
  const COUNT_ON_PAGE = 5;
  let countPages;

  function getCompletedTodo() {
    return todos.filter((item) => item.status === true);
  }

  function getActiveTodo() {
    return todos.filter((item) => item.status === false);
  }

  function filterTodo(val) {
    if (val === 'active') {
      return getActiveTodo();
    }
    if (val === 'completed') {
      return getCompletedTodo();
    }
    return todos;
  }

  function pagination(length) {
    const TOTAL = length;
    countPages = Math.ceil(TOTAL / COUNT_ON_PAGE);
    if (TOTAL > COUNT_ON_PAGE) {
      for (let i = 1; i <= countPages; i += 1) {
        $('.pagination').append(`<li id="${i}" class="page-item"><a class="page-link" href="#">${i}</a></li>`);
      }
    }
  }

  function updateNumberPage() {
    if (currentPage > countPages && countPages !== 0) {
      currentPage = countPages;
    }
  }

  function getTodoCurrentPage(todo) {
    const START_ITEM = (currentPage - 1) * COUNT_ON_PAGE;
    $('.pagination').find(`#${currentPage}`).addClass('active');
    return todo.slice(START_ITEM, START_ITEM + COUNT_ON_PAGE);
  }

  function render() {
    const $ERROR_BLOCK = $('.error');
    const $LIST_TODO = $('#list');
    const $COUNT = $('.todo-count');
    const COUNT_COMPLETED = getCompletedTodo();
    let parts = '';
    $('.pagination').empty();
    const FILTERED_TODO = filterTodo(tabMode);
    pagination(FILTERED_TODO.length);
    updateNumberPage();
    const TODO = getTodoCurrentPage(FILTERED_TODO);
    $ERROR_BLOCK.empty();
    if (error) {
      $ERROR_BLOCK.append('<div class="alert alert-danger"><span>Enter correct value</span></div>');
    }
    $LIST_TODO.empty();
    TODO.forEach((item) => {
      parts += `<li id=${item.id} class="list-group-item">
          <div class="block-li col">
            <input type="checkbox" class="status form-check-input" ${item.status && 'checked'}/>
            <span>${item.value}</span>
            <button class="destroy close" aria-label="Close">x</button>
          </div>
        </li>`;
    });
    $LIST_TODO.html(parts);
    $COUNT.empty();
    $COUNT.html(`<li class="list-group-item d-flex justify-content-between align-items-center">Completed <span class="badge badge-primary badge-pill">${COUNT_COMPLETED.length}</span></li>
      <li class="list-group-item d-flex justify-content-between align-items-center">Active <span class="badge badge-primary badge-pill">${todos.length - COUNT_COMPLETED.length}</span></li>`);
  }

  function validateValue(value) {
    error = false;
    if (!value.trim()) {
      error = true;
    }
  }

  function addTodo() {
    error = false;
    const $INPUT = $('#todo-input');
    let inputValue = $INPUT.val();
    validateValue(inputValue);
    inputValue = _.escape(inputValue);
    if (!error) {
      const TODO = {
        id: Math.random(),
        status: false,
        value: inputValue,
      };
      todos.unshift(TODO);
    }
    currentPage = 1;
    $INPUT.val('');
    render();
  }

  function deleteAllCompletedTodo() {
    todos = todos.filter((item) => item.status === false);
    render();
  }

  function deleteTodo(e) {
    const ID = $(e.target).parents('.list-group-item').attr('id');
    const IDX = todos.findIndex((item) => item.id === Number(ID));
    todos.splice(IDX, 1);
    render();
  }

  function completedTodo(e) {
    const ID = $(e.target).parents('.list-group-item').attr('id');
    const IDX = todos.findIndex((item) => item.id === Number(ID));
    todos[IDX].status = !todos[IDX].status;
    render();
  }

  function completedAllTodo(e) {
    const isChecked = $(e.target).prop('checked');
    if (isChecked) {
      todos.forEach((item) => {
        item.status = true;
      });
    } else {
      todos.forEach((item) => {
        item.status = false;
      });
    }
    render();
  }

  function updateTodo(e) {
    const ID = $(e.target).parent().attr('id');
    const IDX = todos.findIndex((item) => item.id === Number(ID));
    let inputValue = $(e.target).val();
    inputValue = _.escape(inputValue);
    validateValue(inputValue);
    if (!error) {
      todos[IDX].value = inputValue;
    }
    render();
  }

  function showInputForEdit(e) {
    let TODO_TEXT = $(e.currentTarget).children('span').text();
    TODO_TEXT = _.escape(TODO_TEXT);
    $(e.currentTarget).replaceWith(`<input type="text" class="edit-todo form-control" value="${TODO_TEXT}"/>`);
    $('.edit-todo').focus();
  }

  function installTabMode(mode) {
    tabMode = mode;
    $('.filter').children().each(function addOrRemoveClass() {
      $(this).toggleClass('active', tabMode === $(this).attr('id'));
    });
    render();
  }

  function bindEvents() {
    $('#todo-add').on('click', addTodo);
    $('#todo-input').keydown((e) => {
      if (e.key === ENTER_KEY) {
        addTodo();
      }
    });
    $('#todo-destroy').on('click', deleteAllCompletedTodo);
    $('#list').on('click', '.destroy', deleteTodo)
      .on('click', '.status', completedTodo)
      .on('dblclick', '.block-li', showInputForEdit)
      .on('blur', '.edit-todo', updateTodo)
      .keyup((e) => {
        if (e.key === ENTER_KEY) {
          updateTodo(e);
        }
      });
    $('#todo-all-completed').on('click', completedAllTodo);
    $('.filter').on('click', (e) => installTabMode($(e.target).val()));
    $('.pagination').on('click', (e) => {
      currentPage = $(e.target).parent().attr('id');
      render();
    });
  }

  bindEvents();
  $('.filter').children('#all').addClass('active');
});
